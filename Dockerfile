FROM fedora:latest

RUN dnf update -y
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y --profile minimal --default-toolchain nightly
RUN source $HOME/.cargo/env

RUN dnf install -y git \
    meson \
    ninja-build \
    glib2-devel \
    gegl04-devel \
    gobject-introspection-devel \
    babl-devel \
    xorg-x11-server-Xvfb
