// Generated by gir (https://github.com/gtk-rs/gir @ d7c0763cacbc)
// from gir-files (https://github.com/gtk-rs/gir-files @ 4d1189172a70)
// DO NOT EDIT
#![allow(deprecated)]

use crate::{ffi, Rectangle, TileSource};
use glib::{
    prelude::*,
    signal::{connect_raw, SignalHandlerId},
    translate::*,
};
use std::boxed::Box as Box_;

glib::wrapper! {
    #[doc(alias = "GeglTileBackend")]
    pub struct TileBackend(Object<ffi::GeglTileBackend, ffi::GeglTileBackendClass>) @extends TileSource;

    match fn {
        type_ => || ffi::gegl_tile_backend_get_type(),
    }
}

impl TileBackend {
    pub const NONE: Option<&'static TileBackend> = None;

    // rustdoc-stripper-ignore-next
    /// Creates a new builder-pattern struct instance to construct [`TileBackend`] objects.
    ///
    /// This method returns an instance of [`TileBackendBuilder`](crate::builders::TileBackendBuilder) which can be used to create [`TileBackend`] objects.
    pub fn builder() -> TileBackendBuilder {
        TileBackendBuilder::new()
    }

    #[cfg_attr(feature = "v0_4_14", deprecated = "Since 0.4.14")]
    #[allow(deprecated)]
    #[doc(alias = "gegl_tile_backend_unlink_swap")]
    pub fn unlink_swap(path: &str) {
        unsafe {
            ffi::gegl_tile_backend_unlink_swap(path.to_glib_none().0);
        }
    }
}

// rustdoc-stripper-ignore-next
/// A [builder-pattern] type to construct [`TileBackend`] objects.
///
/// [builder-pattern]: https://doc.rust-lang.org/1.0.0/style/ownership/builders.html
#[must_use = "The builder must be built to be used"]
pub struct TileBackendBuilder {
    builder: glib::object::ObjectBuilder<'static, TileBackend>,
}

impl TileBackendBuilder {
    fn new() -> Self {
        Self {
            builder: glib::object::Object::builder(),
        }
    }

    pub fn flush_on_destroy(self, flush_on_destroy: bool) -> Self {
        Self {
            builder: self.builder.property("flush-on-destroy", flush_on_destroy),
        }
    }

    //pub fn format(self, format: /*Unimplemented*/Basic: Pointer) -> Self {
    //    Self { builder: self.builder.property("format", format), }
    //}

    pub fn tile_height(self, tile_height: i32) -> Self {
        Self {
            builder: self.builder.property("tile-height", tile_height),
        }
    }

    pub fn tile_width(self, tile_width: i32) -> Self {
        Self {
            builder: self.builder.property("tile-width", tile_width),
        }
    }

    // rustdoc-stripper-ignore-next
    /// Build the [`TileBackend`].
    #[must_use = "Building the object from the builder is usually expensive and is not expected to have side effects"]
    pub fn build(self) -> TileBackend {
        self.builder.build()
    }
}

mod sealed {
    pub trait Sealed {}
    impl<T: super::IsA<super::TileBackend>> Sealed for T {}
}

pub trait TileBackendExt: IsA<TileBackend> + sealed::Sealed + 'static {
    //#[doc(alias = "gegl_tile_backend_command")]
    //fn command(&self, command: TileCommand, x: i32, y: i32, z: i32, data: /*Unimplemented*/Option<Basic: Pointer>) -> /*Unimplemented*/Option<Basic: Pointer> {
    //    unsafe { TODO: call ffi:gegl_tile_backend_command() }
    //}

    #[doc(alias = "gegl_tile_backend_get_flush_on_destroy")]
    #[doc(alias = "get_flush_on_destroy")]
    #[doc(alias = "flush-on-destroy")]
    fn is_flush_on_destroy(&self) -> bool {
        unsafe {
            from_glib(ffi::gegl_tile_backend_get_flush_on_destroy(
                self.as_ref().to_glib_none().0,
            ))
        }
    }

    #[doc(alias = "gegl_tile_backend_get_tile_height")]
    #[doc(alias = "get_tile_height")]
    #[doc(alias = "tile-height")]
    fn tile_height(&self) -> i32 {
        unsafe { ffi::gegl_tile_backend_get_tile_height(self.as_ref().to_glib_none().0) }
    }

    #[doc(alias = "gegl_tile_backend_get_tile_size")]
    #[doc(alias = "get_tile_size")]
    #[doc(alias = "tile-size")]
    fn tile_size(&self) -> i32 {
        unsafe { ffi::gegl_tile_backend_get_tile_size(self.as_ref().to_glib_none().0) }
    }

    #[doc(alias = "gegl_tile_backend_get_tile_width")]
    #[doc(alias = "get_tile_width")]
    #[doc(alias = "tile-width")]
    fn tile_width(&self) -> i32 {
        unsafe { ffi::gegl_tile_backend_get_tile_width(self.as_ref().to_glib_none().0) }
    }

    #[doc(alias = "gegl_tile_backend_peek_storage")]
    fn peek_storage(&self) -> Option<TileSource> {
        unsafe {
            from_glib_none(ffi::gegl_tile_backend_peek_storage(
                self.as_ref().to_glib_none().0,
            ))
        }
    }

    #[doc(alias = "gegl_tile_backend_set_extent")]
    fn set_extent(&self, rectangle: &Rectangle) {
        unsafe {
            ffi::gegl_tile_backend_set_extent(
                self.as_ref().to_glib_none().0,
                rectangle.to_glib_none().0,
            );
        }
    }

    #[doc(alias = "gegl_tile_backend_set_flush_on_destroy")]
    #[doc(alias = "flush-on-destroy")]
    fn set_flush_on_destroy(&self, flush_on_destroy: bool) {
        unsafe {
            ffi::gegl_tile_backend_set_flush_on_destroy(
                self.as_ref().to_glib_none().0,
                flush_on_destroy.into_glib(),
            );
        }
    }

    //fn format(&self) -> /*Unimplemented*/Basic: Pointer {
    //    ObjectExt::property(self.as_ref(), "format")
    //}

    #[doc(alias = "px-size")]
    fn px_size(&self) -> i32 {
        ObjectExt::property(self.as_ref(), "px-size")
    }

    #[doc(alias = "flush-on-destroy")]
    fn connect_flush_on_destroy_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_flush_on_destroy_trampoline<
            P: IsA<TileBackend>,
            F: Fn(&P) + 'static,
        >(
            this: *mut ffi::GeglTileBackend,
            _param_spec: glib::ffi::gpointer,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(TileBackend::from_glib_borrow(this).unsafe_cast_ref())
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"notify::flush-on-destroy\0".as_ptr() as *const _,
                Some(std::mem::transmute::<*const (), unsafe extern "C" fn()>(
                    notify_flush_on_destroy_trampoline::<Self, F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }

    #[doc(alias = "px-size")]
    fn connect_px_size_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_px_size_trampoline<P: IsA<TileBackend>, F: Fn(&P) + 'static>(
            this: *mut ffi::GeglTileBackend,
            _param_spec: glib::ffi::gpointer,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(TileBackend::from_glib_borrow(this).unsafe_cast_ref())
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"notify::px-size\0".as_ptr() as *const _,
                Some(std::mem::transmute::<*const (), unsafe extern "C" fn()>(
                    notify_px_size_trampoline::<Self, F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }

    #[doc(alias = "tile-size")]
    fn connect_tile_size_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_tile_size_trampoline<
            P: IsA<TileBackend>,
            F: Fn(&P) + 'static,
        >(
            this: *mut ffi::GeglTileBackend,
            _param_spec: glib::ffi::gpointer,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(TileBackend::from_glib_borrow(this).unsafe_cast_ref())
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"notify::tile-size\0".as_ptr() as *const _,
                Some(std::mem::transmute::<*const (), unsafe extern "C" fn()>(
                    notify_tile_size_trampoline::<Self, F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }
}

impl<O: IsA<TileBackend>> TileBackendExt for O {}
