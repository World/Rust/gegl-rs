// Generated by gir (https://github.com/gtk-rs/gir @ d7c0763cacbc)
// from gir-files (https://github.com/gtk-rs/gir-files @ 4d1189172a70)
// DO NOT EDIT
#![allow(deprecated)]

use crate::{ffi, AbyssPolicy, Color, Rectangle, TileBackend, TileHandler, TileSource};
use glib::{
    prelude::*,
    signal::{connect_raw, SignalHandlerId},
    translate::*,
};
use std::boxed::Box as Box_;

glib::wrapper! {
    #[doc(alias = "GeglBuffer")]
    pub struct Buffer(Object<ffi::GeglBuffer>) @extends TileHandler, TileSource;

    match fn {
        type_ => || ffi::gegl_buffer_get_type(),
    }
}

impl Buffer {
    #[doc(alias = "gegl_buffer_introspectable_new")]
    pub fn introspectable_new(
        format_name: &str,
        x: i32,
        y: i32,
        width: i32,
        height: i32,
    ) -> Buffer {
        unsafe {
            from_glib_full(ffi::gegl_buffer_introspectable_new(
                format_name.to_glib_none().0,
                x,
                y,
                width,
                height,
            ))
        }
    }

    //#[doc(alias = "gegl_buffer_linear_new_from_data")]
    //pub fn linear_new_from_data(data: /*Unimplemented*/Option<Basic: Pointer>, format: &babl::Object, extent: &Rectangle, rowstride: i32, destroy_fn_data: /*Unimplemented*/Option<Basic: Pointer>) -> Buffer {
    //    unsafe { TODO: call ffi:gegl_buffer_linear_new_from_data() }
    //}

    #[doc(alias = "gegl_buffer_new_for_backend")]
    #[doc(alias = "new_for_backend")]
    pub fn for_backend(extent: &Rectangle, backend: &impl IsA<TileBackend>) -> Buffer {
        unsafe {
            from_glib_full(ffi::gegl_buffer_new_for_backend(
                extent.to_glib_none().0,
                backend.as_ref().to_glib_none().0,
            ))
        }
    }

    // rustdoc-stripper-ignore-next
    /// Creates a new builder-pattern struct instance to construct [`Buffer`] objects.
    ///
    /// This method returns an instance of [`BufferBuilder`](crate::builders::BufferBuilder) which can be used to create [`Buffer`] objects.
    pub fn builder() -> BufferBuilder {
        BufferBuilder::new()
    }

    //#[doc(alias = "gegl_buffer_add_handler")]
    //pub fn add_handler(&self, handler: /*Unimplemented*/Option<Basic: Pointer>) {
    //    unsafe { TODO: call ffi:gegl_buffer_add_handler() }
    //}

    #[doc(alias = "gegl_buffer_clear")]
    pub fn clear(&self, roi: &Rectangle) {
        unsafe {
            ffi::gegl_buffer_clear(self.to_glib_none().0, roi.to_glib_none().0);
        }
    }

    #[doc(alias = "gegl_buffer_copy")]
    pub fn copy(
        &self,
        src_rect: &Rectangle,
        repeat_mode: AbyssPolicy,
        dst: &Buffer,
        dst_rect: &Rectangle,
    ) {
        unsafe {
            ffi::gegl_buffer_copy(
                self.to_glib_none().0,
                src_rect.to_glib_none().0,
                repeat_mode.into_glib(),
                dst.to_glib_none().0,
                dst_rect.to_glib_none().0,
            );
        }
    }

    #[doc(alias = "gegl_buffer_create_sub_buffer")]
    #[must_use]
    pub fn create_sub_buffer(&self, extent: &Rectangle) -> Option<Buffer> {
        unsafe {
            from_glib_full(ffi::gegl_buffer_create_sub_buffer(
                self.to_glib_none().0,
                extent.to_glib_none().0,
            ))
        }
    }

    #[doc(alias = "gegl_buffer_dup")]
    #[must_use]
    pub fn dup(&self) -> Option<Buffer> {
        unsafe { from_glib_full(ffi::gegl_buffer_dup(self.to_glib_none().0)) }
    }

    #[doc(alias = "gegl_buffer_flush")]
    pub fn flush(&self) {
        unsafe {
            ffi::gegl_buffer_flush(self.to_glib_none().0);
        }
    }

    #[doc(alias = "gegl_buffer_flush_ext")]
    pub fn flush_ext(&self, rect: &Rectangle) {
        unsafe {
            ffi::gegl_buffer_flush_ext(self.to_glib_none().0, rect.to_glib_none().0);
        }
    }

    #[doc(alias = "gegl_buffer_freeze_changed")]
    pub fn freeze_changed(&self) {
        unsafe {
            ffi::gegl_buffer_freeze_changed(self.to_glib_none().0);
        }
    }

    //#[doc(alias = "gegl_buffer_get")]
    //pub fn get(&self, rect: &Rectangle, scale: f64, format: &babl::Object, dest: /*Unimplemented*/Option<Basic: Pointer>, rowstride: i32, repeat_mode: AbyssPolicy) {
    //    unsafe { TODO: call ffi:gegl_buffer_get() }
    //}

    #[doc(alias = "gegl_buffer_get_abyss")]
    #[doc(alias = "get_abyss")]
    pub fn abyss(&self) -> Option<Rectangle> {
        unsafe { from_glib_none(ffi::gegl_buffer_get_abyss(self.to_glib_none().0)) }
    }

    #[doc(alias = "gegl_buffer_get_extent")]
    #[doc(alias = "get_extent")]
    pub fn extent(&self) -> Option<Rectangle> {
        unsafe { from_glib_none(ffi::gegl_buffer_get_extent(self.to_glib_none().0)) }
    }

    //#[doc(alias = "gegl_buffer_get_tile")]
    //#[doc(alias = "get_tile")]
    //pub fn tile(&self, x: i32, y: i32, z: i32) -> /*Ignored*/Option<Tile> {
    //    unsafe { TODO: call ffi:gegl_buffer_get_tile() }
    //}

    #[doc(alias = "gegl_buffer_introspectable_get")]
    pub fn introspectable_get(
        &self,
        rect: &Rectangle,
        scale: f64,
        format_name: Option<&str>,
        repeat_mode: AbyssPolicy,
    ) -> Vec<u8> {
        unsafe {
            let mut data_length = std::mem::MaybeUninit::uninit();
            let ret = FromGlibContainer::from_glib_full_num(
                ffi::gegl_buffer_introspectable_get(
                    self.to_glib_none().0,
                    rect.to_glib_none().0,
                    scale,
                    format_name.to_glib_none().0,
                    repeat_mode.into_glib(),
                    data_length.as_mut_ptr(),
                ),
                data_length.assume_init() as _,
            );
            ret
        }
    }

    #[doc(alias = "gegl_buffer_introspectable_set")]
    pub fn introspectable_set(&self, rect: &Rectangle, format_name: &str, src: &[u8]) {
        let src_length = src.len() as _;
        unsafe {
            ffi::gegl_buffer_introspectable_set(
                self.to_glib_none().0,
                rect.to_glib_none().0,
                format_name.to_glib_none().0,
                src.to_glib_none().0,
                src_length,
            );
        }
    }

    //#[doc(alias = "gegl_buffer_iterator_new")]
    //pub fn iterator_new(&self, roi: &Rectangle, level: i32, format: &babl::Object, access_mode: AccessMode, abyss_policy: AbyssPolicy, max_slots: i32) -> /*Ignored*/Option<BufferIterator> {
    //    unsafe { TODO: call ffi:gegl_buffer_iterator_new() }
    //}

    //#[doc(alias = "gegl_buffer_linear_close")]
    //pub fn linear_close(&self, linear: /*Unimplemented*/Option<Basic: Pointer>) {
    //    unsafe { TODO: call ffi:gegl_buffer_linear_close() }
    //}

    //#[doc(alias = "gegl_buffer_linear_open")]
    //pub fn linear_open(&self, extent: &Rectangle, rowstride: i32, format: &babl::Object) -> /*Unimplemented*/Option<Basic: Pointer> {
    //    unsafe { TODO: call ffi:gegl_buffer_linear_open() }
    //}

    //#[doc(alias = "gegl_buffer_remove_handler")]
    //pub fn remove_handler(&self, handler: /*Unimplemented*/Option<Basic: Pointer>) {
    //    unsafe { TODO: call ffi:gegl_buffer_remove_handler() }
    //}

    //#[doc(alias = "gegl_buffer_sample")]
    //pub fn sample(&self, x: f64, y: f64, scale: /*Ignored*/&mut BufferMatrix2, dest: /*Unimplemented*/Option<Basic: Pointer>, format: &babl::Object, sampler_type: SamplerType, repeat_mode: AbyssPolicy) {
    //    unsafe { TODO: call ffi:gegl_buffer_sample() }
    //}

    //#[doc(alias = "gegl_buffer_sample_at_level")]
    //pub fn sample_at_level(&self, x: f64, y: f64, scale: /*Ignored*/&mut BufferMatrix2, dest: /*Unimplemented*/Option<Basic: Pointer>, format: &babl::Object, level: i32, sampler_type: SamplerType, repeat_mode: AbyssPolicy) {
    //    unsafe { TODO: call ffi:gegl_buffer_sample_at_level() }
    //}

    #[cfg_attr(feature = "v0_4_2", deprecated = "Since 0.4.2")]
    #[allow(deprecated)]
    #[doc(alias = "gegl_buffer_sample_cleanup")]
    pub fn sample_cleanup(&self) {
        unsafe {
            ffi::gegl_buffer_sample_cleanup(self.to_glib_none().0);
        }
    }

    //#[doc(alias = "gegl_buffer_sampler_new")]
    //pub fn sampler_new(&self, format: &babl::Object, sampler_type: SamplerType) -> /*Ignored*/Option<Sampler> {
    //    unsafe { TODO: call ffi:gegl_buffer_sampler_new() }
    //}

    //#[doc(alias = "gegl_buffer_sampler_new_at_level")]
    //pub fn sampler_new_at_level(&self, format: &babl::Object, sampler_type: SamplerType, level: i32) -> /*Ignored*/Option<Sampler> {
    //    unsafe { TODO: call ffi:gegl_buffer_sampler_new_at_level() }
    //}

    #[doc(alias = "gegl_buffer_save")]
    pub fn save(&self, path: &str, roi: &Rectangle) {
        unsafe {
            ffi::gegl_buffer_save(
                self.to_glib_none().0,
                path.to_glib_none().0,
                roi.to_glib_none().0,
            );
        }
    }

    //#[doc(alias = "gegl_buffer_set")]
    //pub fn set(&self, rect: &Rectangle, mipmap_level: i32, format: &babl::Object, src: /*Unimplemented*/Option<Basic: Pointer>, rowstride: i32) {
    //    unsafe { TODO: call ffi:gegl_buffer_set() }
    //}

    #[doc(alias = "gegl_buffer_set_abyss")]
    pub fn set_abyss(&self, abyss: &Rectangle) -> bool {
        unsafe {
            from_glib(ffi::gegl_buffer_set_abyss(
                self.to_glib_none().0,
                abyss.to_glib_none().0,
            ))
        }
    }

    #[doc(alias = "gegl_buffer_set_color")]
    pub fn set_color(&self, rect: &Rectangle, color: &impl IsA<Color>) {
        unsafe {
            ffi::gegl_buffer_set_color(
                self.to_glib_none().0,
                rect.to_glib_none().0,
                color.as_ref().to_glib_none().0,
            );
        }
    }

    //#[doc(alias = "gegl_buffer_set_color_from_pixel")]
    //pub fn set_color_from_pixel(&self, rect: &Rectangle, pixel: /*Unimplemented*/Option<Basic: Pointer>, pixel_format: &babl::Object) {
    //    unsafe { TODO: call ffi:gegl_buffer_set_color_from_pixel() }
    //}

    #[doc(alias = "gegl_buffer_set_extent")]
    pub fn set_extent(&self, extent: &Rectangle) -> bool {
        unsafe {
            from_glib(ffi::gegl_buffer_set_extent(
                self.to_glib_none().0,
                extent.to_glib_none().0,
            ))
        }
    }

    #[doc(alias = "gegl_buffer_set_pattern")]
    pub fn set_pattern(&self, rect: &Rectangle, pattern: &Buffer, x_offset: i32, y_offset: i32) {
        unsafe {
            ffi::gegl_buffer_set_pattern(
                self.to_glib_none().0,
                rect.to_glib_none().0,
                pattern.to_glib_none().0,
                x_offset,
                y_offset,
            );
        }
    }

    #[doc(alias = "gegl_buffer_share_storage")]
    pub fn share_storage(&self, buffer2: &Buffer) -> bool {
        unsafe {
            from_glib(ffi::gegl_buffer_share_storage(
                self.to_glib_none().0,
                buffer2.to_glib_none().0,
            ))
        }
    }

    //#[doc(alias = "gegl_buffer_signal_connect")]
    //pub fn signal_connect<P: FnOnce() + 'static>(&self, detailed_signal: &str, c_handler: P, data: /*Unimplemented*/Option<Basic: Pointer>) -> libc::c_long {
    //    unsafe { TODO: call ffi:gegl_buffer_signal_connect() }
    //}

    #[doc(alias = "gegl_buffer_thaw_changed")]
    pub fn thaw_changed(&self) {
        unsafe {
            ffi::gegl_buffer_thaw_changed(self.to_glib_none().0);
        }
    }

    #[doc(alias = "abyss-height")]
    pub fn abyss_height(&self) -> i32 {
        ObjectExt::property(self, "abyss-height")
    }

    #[doc(alias = "abyss-width")]
    pub fn abyss_width(&self) -> i32 {
        ObjectExt::property(self, "abyss-width")
    }

    #[doc(alias = "abyss-x")]
    pub fn abyss_x(&self) -> i32 {
        ObjectExt::property(self, "abyss-x")
    }

    #[doc(alias = "abyss-y")]
    pub fn abyss_y(&self) -> i32 {
        ObjectExt::property(self, "abyss-y")
    }

    pub fn backend(&self) -> Option<TileBackend> {
        ObjectExt::property(self, "backend")
    }

    //pub fn format(&self) -> /*Unimplemented*/Basic: Pointer {
    //    ObjectExt::property(self, "format")
    //}

    //pub fn set_format(&self, format: /*Unimplemented*/Basic: Pointer) {
    //    ObjectExt::set_property(self,"format", format)
    //}

    pub fn height(&self) -> i32 {
        ObjectExt::property(self, "height")
    }

    pub fn set_height(&self, height: i32) {
        ObjectExt::set_property(self, "height", height)
    }

    pub fn is_initialized(&self) -> bool {
        ObjectExt::property(self, "initialized")
    }

    pub fn path(&self) -> Option<glib::GString> {
        ObjectExt::property(self, "path")
    }

    pub fn pixels(&self) -> i32 {
        ObjectExt::property(self, "pixels")
    }

    #[doc(alias = "px-size")]
    pub fn px_size(&self) -> i32 {
        ObjectExt::property(self, "px-size")
    }

    #[doc(alias = "shift-x")]
    pub fn shift_x(&self) -> i32 {
        ObjectExt::property(self, "shift-x")
    }

    #[doc(alias = "shift-y")]
    pub fn shift_y(&self) -> i32 {
        ObjectExt::property(self, "shift-y")
    }

    #[doc(alias = "tile-height")]
    pub fn tile_height(&self) -> i32 {
        ObjectExt::property(self, "tile-height")
    }

    #[doc(alias = "tile-width")]
    pub fn tile_width(&self) -> i32 {
        ObjectExt::property(self, "tile-width")
    }

    pub fn width(&self) -> i32 {
        ObjectExt::property(self, "width")
    }

    pub fn set_width(&self, width: i32) {
        ObjectExt::set_property(self, "width", width)
    }

    pub fn x(&self) -> i32 {
        ObjectExt::property(self, "x")
    }

    pub fn set_x(&self, x: i32) {
        ObjectExt::set_property(self, "x", x)
    }

    pub fn y(&self) -> i32 {
        ObjectExt::property(self, "y")
    }

    pub fn set_y(&self, y: i32) {
        ObjectExt::set_property(self, "y", y)
    }

    #[doc(alias = "gegl_buffer_load")]
    pub fn load(path: &str) -> Option<Buffer> {
        unsafe { from_glib_full(ffi::gegl_buffer_load(path.to_glib_none().0)) }
    }

    #[doc(alias = "gegl_buffer_open")]
    pub fn open(path: &str) -> Option<Buffer> {
        unsafe { from_glib_full(ffi::gegl_buffer_open(path.to_glib_none().0)) }
    }

    #[doc(alias = "gegl_buffer_swap_create_file")]
    pub fn swap_create_file(suffix: Option<&str>) -> Option<std::path::PathBuf> {
        unsafe { from_glib_full(ffi::gegl_buffer_swap_create_file(suffix.to_glib_none().0)) }
    }

    #[doc(alias = "gegl_buffer_swap_has_file")]
    pub fn swap_has_file(path: impl AsRef<std::path::Path>) -> bool {
        unsafe {
            from_glib(ffi::gegl_buffer_swap_has_file(
                path.as_ref().to_glib_none().0,
            ))
        }
    }

    #[doc(alias = "gegl_buffer_swap_remove_file")]
    pub fn swap_remove_file(path: impl AsRef<std::path::Path>) {
        unsafe {
            ffi::gegl_buffer_swap_remove_file(path.as_ref().to_glib_none().0);
        }
    }

    #[doc(alias = "changed")]
    pub fn connect_changed<F: Fn(&Self, &Rectangle) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn changed_trampoline<F: Fn(&Buffer, &Rectangle) + 'static>(
            this: *mut ffi::GeglBuffer,
            object: *mut ffi::GeglRectangle,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this), &from_glib_borrow(object))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"changed\0".as_ptr() as *const _,
                Some(std::mem::transmute::<*const (), unsafe extern "C" fn()>(
                    changed_trampoline::<F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }

    #[doc(alias = "format")]
    pub fn connect_format_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_format_trampoline<F: Fn(&Buffer) + 'static>(
            this: *mut ffi::GeglBuffer,
            _param_spec: glib::ffi::gpointer,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"notify::format\0".as_ptr() as *const _,
                Some(std::mem::transmute::<*const (), unsafe extern "C" fn()>(
                    notify_format_trampoline::<F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }

    #[doc(alias = "height")]
    pub fn connect_height_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_height_trampoline<F: Fn(&Buffer) + 'static>(
            this: *mut ffi::GeglBuffer,
            _param_spec: glib::ffi::gpointer,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"notify::height\0".as_ptr() as *const _,
                Some(std::mem::transmute::<*const (), unsafe extern "C" fn()>(
                    notify_height_trampoline::<F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }

    #[doc(alias = "pixels")]
    pub fn connect_pixels_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_pixels_trampoline<F: Fn(&Buffer) + 'static>(
            this: *mut ffi::GeglBuffer,
            _param_spec: glib::ffi::gpointer,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"notify::pixels\0".as_ptr() as *const _,
                Some(std::mem::transmute::<*const (), unsafe extern "C" fn()>(
                    notify_pixels_trampoline::<F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }

    #[doc(alias = "px-size")]
    pub fn connect_px_size_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_px_size_trampoline<F: Fn(&Buffer) + 'static>(
            this: *mut ffi::GeglBuffer,
            _param_spec: glib::ffi::gpointer,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"notify::px-size\0".as_ptr() as *const _,
                Some(std::mem::transmute::<*const (), unsafe extern "C" fn()>(
                    notify_px_size_trampoline::<F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }

    #[doc(alias = "width")]
    pub fn connect_width_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_width_trampoline<F: Fn(&Buffer) + 'static>(
            this: *mut ffi::GeglBuffer,
            _param_spec: glib::ffi::gpointer,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"notify::width\0".as_ptr() as *const _,
                Some(std::mem::transmute::<*const (), unsafe extern "C" fn()>(
                    notify_width_trampoline::<F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }

    #[doc(alias = "x")]
    pub fn connect_x_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_x_trampoline<F: Fn(&Buffer) + 'static>(
            this: *mut ffi::GeglBuffer,
            _param_spec: glib::ffi::gpointer,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"notify::x\0".as_ptr() as *const _,
                Some(std::mem::transmute::<*const (), unsafe extern "C" fn()>(
                    notify_x_trampoline::<F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }

    #[doc(alias = "y")]
    pub fn connect_y_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_y_trampoline<F: Fn(&Buffer) + 'static>(
            this: *mut ffi::GeglBuffer,
            _param_spec: glib::ffi::gpointer,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"notify::y\0".as_ptr() as *const _,
                Some(std::mem::transmute::<*const (), unsafe extern "C" fn()>(
                    notify_y_trampoline::<F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }
}

// rustdoc-stripper-ignore-next
/// A [builder-pattern] type to construct [`Buffer`] objects.
///
/// [builder-pattern]: https://doc.rust-lang.org/1.0.0/style/ownership/builders.html
#[must_use = "The builder must be built to be used"]
pub struct BufferBuilder {
    builder: glib::object::ObjectBuilder<'static, Buffer>,
}

impl BufferBuilder {
    fn new() -> Self {
        Self {
            builder: glib::object::Object::builder(),
        }
    }

    pub fn abyss_height(self, abyss_height: i32) -> Self {
        Self {
            builder: self.builder.property("abyss-height", abyss_height),
        }
    }

    pub fn abyss_width(self, abyss_width: i32) -> Self {
        Self {
            builder: self.builder.property("abyss-width", abyss_width),
        }
    }

    pub fn abyss_x(self, abyss_x: i32) -> Self {
        Self {
            builder: self.builder.property("abyss-x", abyss_x),
        }
    }

    pub fn abyss_y(self, abyss_y: i32) -> Self {
        Self {
            builder: self.builder.property("abyss-y", abyss_y),
        }
    }

    pub fn backend(self, backend: &impl IsA<TileBackend>) -> Self {
        Self {
            builder: self.builder.property("backend", backend.clone().upcast()),
        }
    }

    //pub fn format(self, format: /*Unimplemented*/Basic: Pointer) -> Self {
    //    Self { builder: self.builder.property("format", format), }
    //}

    pub fn height(self, height: i32) -> Self {
        Self {
            builder: self.builder.property("height", height),
        }
    }

    pub fn initialized(self, initialized: bool) -> Self {
        Self {
            builder: self.builder.property("initialized", initialized),
        }
    }

    pub fn path(self, path: impl Into<glib::GString>) -> Self {
        Self {
            builder: self.builder.property("path", path.into()),
        }
    }

    pub fn shift_x(self, shift_x: i32) -> Self {
        Self {
            builder: self.builder.property("shift-x", shift_x),
        }
    }

    pub fn shift_y(self, shift_y: i32) -> Self {
        Self {
            builder: self.builder.property("shift-y", shift_y),
        }
    }

    pub fn tile_height(self, tile_height: i32) -> Self {
        Self {
            builder: self.builder.property("tile-height", tile_height),
        }
    }

    pub fn tile_width(self, tile_width: i32) -> Self {
        Self {
            builder: self.builder.property("tile-width", tile_width),
        }
    }

    pub fn width(self, width: i32) -> Self {
        Self {
            builder: self.builder.property("width", width),
        }
    }

    pub fn x(self, x: i32) -> Self {
        Self {
            builder: self.builder.property("x", x),
        }
    }

    pub fn y(self, y: i32) -> Self {
        Self {
            builder: self.builder.property("y", y),
        }
    }

    //pub fn source(self, source: &impl IsA</*Ignored*/glib::Object>) -> Self {
    //    Self { builder: self.builder.property("source", source.clone().upcast()), }
    //}

    // rustdoc-stripper-ignore-next
    /// Build the [`Buffer`].
    #[must_use = "Building the object from the builder is usually expensive and is not expected to have side effects"]
    pub fn build(self) -> Buffer {
        self.builder.build()
    }
}
