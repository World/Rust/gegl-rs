// Generated by gir (https://github.com/gtk-rs/gir @ d7c0763cacbc)
// from gir-files (https://github.com/gtk-rs/gir-files @ 4d1189172a70)
// DO NOT EDIT

use crate::{ffi, Buffer, RectangleAlignment};
use glib::translate::*;

glib::wrapper! {
    pub struct Rectangle(BoxedInline<ffi::GeglRectangle>);

    match fn {
        copy => |ptr| glib::gobject_ffi::g_boxed_copy(ffi::gegl_rectangle_get_type(), ptr as *mut _) as *mut ffi::GeglRectangle,
        free => |ptr| glib::gobject_ffi::g_boxed_free(ffi::gegl_rectangle_get_type(), ptr as *mut _),
        type_ => || ffi::gegl_rectangle_get_type(),
    }
}

impl Rectangle {
    #[doc(alias = "gegl_rectangle_new")]
    pub fn new(x: i32, y: i32, width: u32, height: u32) -> Rectangle {
        unsafe { from_glib_full(ffi::gegl_rectangle_new(x, y, width, height)) }
    }

    #[doc(alias = "gegl_rectangle_align")]
    pub fn align(
        &mut self,
        rectangle: &Rectangle,
        tile: &Rectangle,
        alignment: RectangleAlignment,
    ) -> bool {
        unsafe {
            from_glib(ffi::gegl_rectangle_align(
                self.to_glib_none_mut().0,
                rectangle.to_glib_none().0,
                tile.to_glib_none().0,
                alignment.into_glib(),
            ))
        }
    }

    #[doc(alias = "gegl_rectangle_align_to_buffer")]
    pub fn align_to_buffer(
        &mut self,
        rectangle: &Rectangle,
        buffer: &Buffer,
        alignment: RectangleAlignment,
    ) -> bool {
        unsafe {
            from_glib(ffi::gegl_rectangle_align_to_buffer(
                self.to_glib_none_mut().0,
                rectangle.to_glib_none().0,
                buffer.to_glib_none().0,
                alignment.into_glib(),
            ))
        }
    }

    #[doc(alias = "gegl_rectangle_bounding_box")]
    pub fn bounding_box(&mut self, source1: &Rectangle, source2: &Rectangle) {
        unsafe {
            ffi::gegl_rectangle_bounding_box(
                self.to_glib_none_mut().0,
                source1.to_glib_none().0,
                source2.to_glib_none().0,
            );
        }
    }

    #[doc(alias = "gegl_rectangle_contains")]
    pub fn contains(&self, child: &Rectangle) -> bool {
        unsafe {
            from_glib(ffi::gegl_rectangle_contains(
                self.to_glib_none().0,
                child.to_glib_none().0,
            ))
        }
    }

    #[doc(alias = "gegl_rectangle_dump")]
    pub fn dump(&self) {
        unsafe {
            ffi::gegl_rectangle_dump(self.to_glib_none().0);
        }
    }

    #[doc(alias = "gegl_rectangle_dup")]
    #[must_use]
    pub fn dup(&self) -> Option<Rectangle> {
        unsafe { from_glib_full(ffi::gegl_rectangle_dup(self.to_glib_none().0)) }
    }

    #[doc(alias = "gegl_rectangle_equal")]
    fn equal(&self, rectangle2: &Rectangle) -> bool {
        unsafe {
            from_glib(ffi::gegl_rectangle_equal(
                self.to_glib_none().0,
                rectangle2.to_glib_none().0,
            ))
        }
    }

    #[doc(alias = "gegl_rectangle_equal_coords")]
    pub fn equal_coords(&self, x: i32, y: i32, width: i32, height: i32) -> bool {
        unsafe {
            from_glib(ffi::gegl_rectangle_equal_coords(
                self.to_glib_none().0,
                x,
                y,
                width,
                height,
            ))
        }
    }

    #[doc(alias = "gegl_rectangle_intersect")]
    pub fn intersect(&mut self, src1: &Rectangle, src2: &Rectangle) -> bool {
        unsafe {
            from_glib(ffi::gegl_rectangle_intersect(
                self.to_glib_none_mut().0,
                src1.to_glib_none().0,
                src2.to_glib_none().0,
            ))
        }
    }

    #[doc(alias = "gegl_rectangle_is_empty")]
    pub fn is_empty(&self) -> bool {
        unsafe { from_glib(ffi::gegl_rectangle_is_empty(self.to_glib_none().0)) }
    }

    #[doc(alias = "gegl_rectangle_is_infinite_plane")]
    pub fn is_infinite_plane(&self) -> bool {
        unsafe { from_glib(ffi::gegl_rectangle_is_infinite_plane(self.to_glib_none().0)) }
    }

    #[doc(alias = "gegl_rectangle_set")]
    pub fn set(&mut self, x: i32, y: i32, width: u32, height: u32) {
        unsafe {
            ffi::gegl_rectangle_set(self.to_glib_none_mut().0, x, y, width, height);
        }
    }

    #[doc(alias = "gegl_rectangle_subtract")]
    pub fn subtract(&mut self, minuend: &Rectangle, subtrahend: &Rectangle) -> i32 {
        unsafe {
            ffi::gegl_rectangle_subtract(
                self.to_glib_none_mut().0,
                minuend.to_glib_none().0,
                subtrahend.to_glib_none().0,
            )
        }
    }

    #[doc(alias = "gegl_rectangle_subtract_bounding_box")]
    pub fn subtract_bounding_box(&mut self, minuend: &Rectangle, subtrahend: &Rectangle) -> bool {
        unsafe {
            from_glib(ffi::gegl_rectangle_subtract_bounding_box(
                self.to_glib_none_mut().0,
                minuend.to_glib_none().0,
                subtrahend.to_glib_none().0,
            ))
        }
    }

    #[doc(alias = "gegl_rectangle_xor")]
    pub fn xor(&mut self, source1: &Rectangle, source2: &Rectangle) -> i32 {
        unsafe {
            ffi::gegl_rectangle_xor(
                self.to_glib_none_mut().0,
                source1.to_glib_none().0,
                source2.to_glib_none().0,
            )
        }
    }
}

impl PartialEq for Rectangle {
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        self.equal(other)
    }
}

impl Eq for Rectangle {}
