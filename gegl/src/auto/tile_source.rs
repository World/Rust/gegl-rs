// Generated by gir (https://github.com/gtk-rs/gir @ d7c0763cacbc)
// from gir-files (https://github.com/gtk-rs/gir-files @ 4d1189172a70)
// DO NOT EDIT

use crate::ffi;

glib::wrapper! {
    #[doc(alias = "GeglTileSource")]
    pub struct TileSource(Object<ffi::GeglTileSource, ffi::GeglTileSourceClass>);

    match fn {
        type_ => || ffi::gegl_tile_source_get_type(),
    }
}

impl TileSource {
    pub const NONE: Option<&'static TileSource> = None;
}
