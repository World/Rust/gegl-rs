use babl::ObjectType;
use glib::translate::*;

use crate::{BlitFlags, Node, Rectangle};

impl Node {
    #[doc(alias = "gegl_node_blit")]
    pub fn blit(
        &self,
        scale: f64,
        roi: &Rectangle,
        format: &babl::Format,
        destination_buf: Option<&mut [u8]>,
        rowstride: i32,
        flags: BlitFlags,
    ) {
        if destination_buf.is_some() {
            assert!(
                (format.bytes_per_pixel() * roi.width() * roi.height()) as usize
                    <= destination_buf.as_ref().unwrap().len()
            );
        }
        unsafe {
            ffi::gegl_node_blit(
                self.to_glib_none().0,
                scale,
                roi.to_glib_none().0,
                format.inner(),
                destination_buf
                    .map(|buf| buf.as_ptr() as *mut std::ffi::c_void)
                    .unwrap_or_else(std::ptr::null_mut),
                rowstride,
                flags.bits(),
            )
        }
    }

    #[doc(alias = "gegl_node_new_child")]
    #[must_use]
    pub fn new_child(
        &self,
        operation: Option<&str>,
        props: &[(&str, glib::Value)],
    ) -> Option<Node> {
        operation
            .and_then(|operation| self.create_child(operation))
            .or_else(|| self.add_child(&Node::new()))
            .map(|node| {
                for prop in props {
                    node.set_property(prop.0, &prop.1)
                }

                node
            })
    }

    #[doc(alias = "gegl_node_link_many")]
    pub fn link_many(&self, dests: &[&Self]) {
        let mut source = self;

        for dest in dests {
            source.link(&dest);
            source = dest;
        }
    }

    #[doc(alias = "get_bounding_box")]
    pub fn bounding_box(&self) -> Rectangle {
        unsafe {
            Rectangle {
                inner: ffi::gegl_node_get_bounding_box(self.to_glib_none().0),
            }
        }
    }
}
