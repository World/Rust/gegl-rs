# GEGL (Generic Imaging Library) Rust bindings

The project provides bindings for the following libraries: [GEGL](https://gitlab.gnome.org/GNOME/gegl)  & [Babl](https://gitlab.gnome.org/GNOME/babl).

It's currently a WIP, help testing it is appreciated.

Currently it use the last released version of glib-rs and a manually patched Gir file for GEGL.

## Documentation

- gegl: <https://world.pages.gitlab.gnome.org/Rust/gegl-rs/gegl>
- gegl-sys: <https://world.pages.gitlab.gnome.org/Rust/gegl-rs/gegl_sys>
- babl: <https://world.pages.gitlab.gnome.org/Rust/gegl-rs/babl>
- babl-sys: <https://world.pages.gitlab.gnome.org/Rust/gegl-rs/babl_sys>

## Maintainers

Bilal Elmoussaoui <bil.elmoussaoui@gmail.com>
Hubert Figuière <hub@figuiere.net>

### Contributors

PolyMeilex <marynczakbartlomiej@gmail.com>
krzygorz <krzygorz@gmail.com>
