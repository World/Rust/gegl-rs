/// # Safety
///
/// The trait is only meant to be implemented internally by Babl types
pub unsafe trait ObjectType {
    #[allow(clippy::missing_safety_doc)]
    unsafe fn from_raw_full(ptr: *const ::ffi::Babl) -> Self;
    #[allow(clippy::missing_safety_doc)]
    unsafe fn inner(&self) -> *const ::ffi::Babl;

    #[doc(alias = "get_name")]
    #[doc(alias = "babl_get_name")]
    fn name(&self) -> String {
        unsafe {
            std::ffi::CStr::from_ptr(ffi::babl_get_name(self.inner()))
                .to_string_lossy()
                .into()
        }
    }

    #[doc(alias = "babl_introspect")]
    fn introspect(&self) {
        unsafe { ffi::babl_introspect(self.inner()) }
    }
}

macro_rules! define_object {
    ($rust_type:ident) => {
        #[derive(Debug)]
        pub struct $rust_type(*const ffi::Babl);

        unsafe impl crate::ObjectType for $rust_type {
            unsafe fn from_raw_full(ptr: *const ffi::Babl) -> Self {
                assert!(!ptr.is_null());
                Self(ptr)
            }

            unsafe fn inner(&self) -> *const ffi::Babl {
                self.0
            }
        }
    };
}

/*#[derive(Debug)]
pub struct Object(*const ffi::Babl);

impl Object {


    /* Palette */

    pub fn new_palette(
        name: &str,
        format_u8: &mut Object,
        format_u8_with_alpha: &mut Object,
    ) -> Self {
        unsafe {
            let c_name = CString::new(name).unwrap();
            Self::from_raw_full(ffi::babl_new_palette(
                c_name.as_ptr() as *const i8,
                &mut format_u8.0,
                &mut format_u8_with_alpha.0,
            ))
        }
    }

    pub fn palette_reset(babl: &Object) {
        unsafe { ffi::babl_palette_reset(babl.0) }
    }

    /* Conversion */
    pub fn conversion_get_source_space(conversion: &Object) -> Self {
        unsafe { Self::from_raw_full(ffi::babl_conversion_get_source_space(conversion.0)) }
    }

    pub fn conversion_get_destination_space(conversion: &Object) -> Self {
        unsafe { Self::from_raw_full(ffi::babl_conversion_get_destination_space(conversion.0)) }
    }
}
*/
