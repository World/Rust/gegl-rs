use bitflags::bitflags;

pub enum Error {
    Space(String),
}

bitflags! {
    #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
    pub struct ModelFlag: u32 {
        /// the model encodes alpha.
        #[doc(alias = "BablModelFlag_BABL_MODEL_FLAG_ALPHA")]
        const ALPHA = ffi::BablModelFlag_BABL_MODEL_FLAG_ALPHA;

        /// the alpha is associated alpha.
        #[doc(alias = "BablModelFlag_BABL_MODEL_FLAG_ASSOCIATED")]
        const ASSOCIATED = ffi::BablModelFlag_BABL_MODEL_FLAG_ASSOCIATED;

        /// the components are inverted (used for getting the additive complement space of CMYK).
        #[doc(alias = "BablModelFlag_BABL_MODEL_FLAG_INVERTED")]
        const INVERTED = ffi::BablModelFlag_BABL_MODEL_FLAG_INVERTED;

        /// the data has no TRC, i.e. is linear
        #[doc(alias = "BablModelFlag_BABL_MODEL_FLAG_LINEAR")]
        const LINEAR = ffi::BablModelFlag_BABL_MODEL_FLAG_LINEAR;

        /// the data has a TRC - the TRC from the configured space
        #[doc(alias = "BablModelFlag_BABL_MODEL_FLAG_NONLINEAR")]
        const NON_LINEAR = ffi::BablModelFlag_BABL_MODEL_FLAG_NONLINEAR;

        /// the data has a TRC - a perceptual TRC where 50% gray is 0.5
        #[doc(alias = "BablModelFlag_BABL_MODEL_FLAG_PERCEPTUAL")]
        const PERCEPTUAL = ffi::BablModelFlag_BABL_MODEL_FLAG_PERCEPTUAL;

        /// this is a gray component model
        #[doc(alias = "BablModelFlag_BABL_MODEL_FLAG_GRAY")]
        const GREY = ffi::BablModelFlag_BABL_MODEL_FLAG_GRAY;

        /// this is an RGB based component model, the space associated is expected to contain an RGB matrix profile.
        #[doc(alias = "BablModelFlag_BABL_MODEL_FLAG_RGB")]
        const RGB = ffi::BablModelFlag_BABL_MODEL_FLAG_RGB;

        /// this model is part of the CIE family of spaces
        #[doc(alias = "BablModelFlag_BABL_MODEL_FLAG_CIE")]
        const CIE = ffi::BablModelFlag_BABL_MODEL_FLAG_CIE;

        /// the encodings described are CMYK encodings, the space associated is expected to contain an CMYK ICC profile.
        #[doc(alias = "BablModelFlag_BABL_MODEL_FLAG_CMYK")]
        const CMYK = ffi::BablModelFlag_BABL_MODEL_FLAG_CMYK;
    }
}

#[derive(Debug, Clone, Copy)]
pub enum SpaceFlags {
    #[doc(alias = "BablSpaceFlags_BABL_SPACE_FLAG_NONE")]
    None,
    #[doc(alias = "BablSpaceFlags_BABL_SPACE_FLAG_EQUALIZE")]
    Equalize,
    __Unknown(u32),
}

impl From<ffi::BablSpaceFlags> for SpaceFlags {
    fn from(space_flags: ffi::BablSpaceFlags) -> Self {
        match space_flags {
            ffi::BablSpaceFlags_BABL_SPACE_FLAG_NONE => Self::None,
            ffi::BablSpaceFlags_BABL_SPACE_FLAG_EQUALIZE => Self::Equalize,
            u => Self::__Unknown(u),
        }
    }
}

impl From<SpaceFlags> for ffi::BablSpaceFlags {
    fn from(space: SpaceFlags) -> Self {
        match space {
            SpaceFlags::Equalize => ffi::BablSpaceFlags_BABL_SPACE_FLAG_EQUALIZE,
            SpaceFlags::None => ffi::BablSpaceFlags_BABL_SPACE_FLAG_NONE,
            SpaceFlags::__Unknown(u) => u,
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub enum IccIntent {
    #[doc(alias = "BablIccIntent_BABL_ICC_INTENT_PERCEPTUAL")]
    Perceptual,
    #[doc(alias = "BablIccIntent_BABL_ICC_INTENT_RELATIVE_COLORIMETRIC")]
    RelativeColorimetric,
    #[doc(alias = "BablIccIntent_BABL_ICC_INTENT_SATURATION")]
    Saturation,
    #[doc(alias = "BablIccIntent_BABL_ICC_INTENT_ABSOLUTE_COLORIMETRIC")]
    AbsoluteColorimetric,
    #[doc(alias = "BablIccIntent_BABL_ICC_INTENT_PERFORMANCE")]
    Performance,
    __Unknown(u32),
}

impl From<ffi::BablIccIntent> for IccIntent {
    fn from(icc_intent: ffi::BablIccIntent) -> Self {
        match icc_intent {
            ffi::BablIccIntent_BABL_ICC_INTENT_PERCEPTUAL => Self::Perceptual,
            ffi::BablIccIntent_BABL_ICC_INTENT_RELATIVE_COLORIMETRIC => Self::RelativeColorimetric,
            ffi::BablIccIntent_BABL_ICC_INTENT_SATURATION => Self::Saturation,
            ffi::BablIccIntent_BABL_ICC_INTENT_ABSOLUTE_COLORIMETRIC => Self::AbsoluteColorimetric,
            ffi::BablIccIntent_BABL_ICC_INTENT_PERFORMANCE => Self::Performance,
            u => Self::__Unknown(u),
        }
    }
}

impl From<IccIntent> for ffi::BablIccIntent {
    fn from(icc_intent: IccIntent) -> Self {
        match icc_intent {
            IccIntent::Perceptual => ffi::BablIccIntent_BABL_ICC_INTENT_PERCEPTUAL,
            IccIntent::RelativeColorimetric => {
                ffi::BablIccIntent_BABL_ICC_INTENT_RELATIVE_COLORIMETRIC
            }
            IccIntent::Saturation => ffi::BablIccIntent_BABL_ICC_INTENT_SATURATION,
            IccIntent::AbsoluteColorimetric => {
                ffi::BablIccIntent_BABL_ICC_INTENT_ABSOLUTE_COLORIMETRIC
            }
            IccIntent::Performance => ffi::BablIccIntent_BABL_ICC_INTENT_PERFORMANCE,
            IccIntent::__Unknown(u) => u,
        }
    }
}
