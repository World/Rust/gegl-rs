#[doc(alias = "get_version")]
#[doc(alias = "babl_get_version")]
pub fn version() -> (i32, i32, i32) {
    unsafe {
        let mut major: i32 = 0;
        let mut minor: i32 = 0;
        let mut micro: i32 = 0;

        ffi::babl_get_version(&mut major, &mut minor, &mut micro);
        (major, minor, micro)
    }
}

#[doc(alias = "babl_init")]
pub fn init() {
    static INIT: std::sync::Once = std::sync::Once::new();

    INIT.call_once(|| unsafe {
        ffi::babl_init();
    });
}

#[doc(alias = "babl_exit")]
#[deprecated(note = "init() is once, so calling exit is unwanted")]
pub fn exit() {
    // We disable calling babl_exit.
    // unsafe {
    //     ffi::babl_exit();
    // }
}

#[cfg(test)]
mod tests {
    use super::version;

    #[test]
    fn test_version() {
        let config = system_deps::Config::new().probe().unwrap();

        let dep = config
            .get_by_name("babl")
            .or_else(|| config.get_by_name("babl-0.1"))
            .unwrap();
        let v = version();
        assert!(matches!(
            version_compare::compare(&dep.version, &format!("{}.{}.{}", v.0, v.1, v.2)),
            Ok(version_compare::Cmp::Eq)
        ));
    }
}
