use crate::{Error, IccIntent, ObjectType, SpaceFlags, Trc};

define_object!(Space);

impl Space {
    #[doc(alias = "babl_space")]
    pub fn from_name(name: &str) -> Self {
        unsafe { Self::from_raw_full(ffi::babl_space(name.as_ptr() as *const std::ffi::c_char)) }
    }

    #[doc(alias = "babl_space_from_icc")]
    pub fn from_icc(icc_data: Vec<u8>, intent: IccIntent) -> Result<Self, Error> {
        unsafe {
            let error = "";

            let space = ffi::babl_space_from_icc(
                icc_data.as_ptr() as *const std::ffi::c_char,
                icc_data.len() as _,
                intent.into(),
                error.as_ptr() as *mut *const std::ffi::c_char,
            );

            match error {
                "" => Ok(Self::from_raw_full(space)),
                err => Err(Error::Space(err.to_string())),
            }
        }
    }

    #[allow(clippy::too_many_arguments)]
    #[doc(alias = "babl_space_from_chromaticities")]
    pub fn from_chromaticities(
        name: &str,
        wx: f64,
        wy: f64,
        rx: f64,
        ry: f64,
        gx: f64,
        gy: f64,
        bx: f64,
        by: f64,
        trc_red: &Trc,
        trc_green: &Trc,
        trc_blue: &Trc,
        flags: SpaceFlags,
    ) -> Self {
        unsafe {
            Self::from_raw_full(ffi::babl_space_from_chromaticities(
                name.as_ptr() as *const std::ffi::c_char,
                wx,
                wy,
                rx,
                ry,
                gx,
                gy,
                bx,
                by,
                trc_red.inner(),
                trc_green.inner(),
                trc_blue.inner(),
                flags.into(),
            ))
        }
    }

    #[allow(clippy::too_many_arguments)]
    #[doc(alias = "babl_space_from_rgbxyz_matrix")]
    pub fn from_rgbxyz_matrix(
        name: &str,
        wx: f64,
        wy: f64,
        wz: f64,
        rx: f64,
        gx: f64,
        bx: f64,
        ry: f64,
        gy: f64,
        by: f64,
        rz: f64,
        gz: f64,
        bz: f64,
        trc_red: &Trc,
        trc_green: &Trc,
        trc_blue: &Trc,
    ) -> Self {
        unsafe {
            Self::from_raw_full(ffi::babl_space_from_rgbxyz_matrix(
                name.as_ptr() as *const std::ffi::c_char,
                wx,
                wy,
                wz,
                rx,
                gx,
                bx,
                ry,
                gy,
                by,
                rz,
                gz,
                bz,
                trc_red.inner(),
                trc_green.inner(),
                trc_blue.inner(),
            ))
        }
    }

    #[doc(alias = "babl_space_with_trc")]
    pub fn with_trc(self, trc: &Trc) -> Self {
        unsafe { Self::from_raw_full(ffi::babl_space_with_trc(self.0, trc.inner())) }
    }

    #[doc(alias = "get_gamma")]
    #[doc(alias = "babl_space_get_gamma")]
    pub fn gamma(&self) -> f64 {
        unsafe { ffi::babl_space_get_gamma(self.0) }
    }

    #[doc(alias = "get_rgb_luminance")]
    #[doc(alias = "babl_space_get_rgb_luminance")]
    pub fn rgb_luminance(&self) -> (f64, f64, f64) {
        unsafe {
            let mut luminance = (0.0, 0.0, 0.0);
            ffi::babl_space_get_rgb_luminance(
                self.0,
                &mut luminance.0,
                &mut luminance.1,
                &mut luminance.2,
            );
            (luminance.0, luminance.1, luminance.2)
        }
    }

    #[doc(alias = "babl_space_is_cmyk")]
    pub fn is_cmyk(&self) -> bool {
        unsafe { ffi::babl_space_is_cmyk(self.0) == 1 }
    }

    #[doc(alias = "babl_space_is_gray")]
    pub fn is_gray(&self) -> bool {
        unsafe { ffi::babl_space_is_gray(self.0) == 1 }
    }

    #[allow(clippy::too_many_arguments)]
    #[doc(alias = "babl_space_get")]
    pub fn get(
        &self,
        xw: &mut f64,
        yw: &mut f64,
        xr: &mut f64,
        yr: &mut f64,
        xg: &mut f64,
        yg: &mut f64,
        xb: &mut f64,
        yb: &mut f64,
        red_trc: &mut Trc,
        green_trc: &mut Trc,
        blue_trc: &mut Trc,
    ) {
        unsafe {
            ffi::babl_space_get(
                self.0,
                xw,
                yw,
                xr,
                yr,
                xg,
                yg,
                xb,
                yb,
                &mut red_trc.inner(),
                &mut green_trc.inner(),
                &mut blue_trc.inner(),
            )
        }
    }
}
