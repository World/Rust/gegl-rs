use crate::ObjectType;

define_object!(Type);

impl Type {
    #[doc(alias = "babl_type")]
    pub fn from_type(data_type: &str) -> Self {
        unsafe {
            Self::from_raw_full(ffi::babl_type(data_type.as_ptr() as *const std::ffi::c_char))
        }
    }
}
