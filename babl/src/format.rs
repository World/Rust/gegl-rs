use crate::{Model, ObjectType, Space, Type};
use std::ffi::CStr;

define_object!(Format);

impl Format {
    /// Returns the babl object representing the color format given by
    ///
    /// @name such as for example \"RGB u8\", \"CMYK float\" or \"CIE Lab u16\",
    /// creates a format using the sRGB space, to also specify the color space
    /// and TRCs for a format, see babl_format_with_space.
    #[doc(alias = "babl_format")]
    pub fn from_encoding(encoding: &str) -> Self {
        let encoding = std::ffi::CString::new(encoding).unwrap();
        unsafe {
            Self::from_raw_full(ffi::babl_format(
                encoding.as_ptr() as *const std::ffi::c_char
            ))
        }
    }

    #[doc(alias = "babl_format_with_space")]
    pub fn format_with_space(encoding: &str, space: &Space) -> Self {
        let encoding = std::ffi::CString::new(encoding).unwrap();
        unsafe {
            Self::from_raw_full(ffi::babl_format_with_space(
                encoding.as_ptr() as *const std::ffi::c_char,
                space.inner(),
            ))
        }
    }

    #[doc(alias = "babl_format_exists")]
    pub fn exists(format_name: &str) -> bool {
        let format_name = std::ffi::CString::new(format_name).unwrap();
        unsafe { ffi::babl_format_exists(format_name.as_ptr() as *const std::ffi::c_char) == 1 }
    }

    #[doc(alias = "get_bytes_per_pixel")]
    #[doc(alias = "babl_format_get_bytes_per_pixel")]
    pub fn bytes_per_pixel(&self) -> i32 {
        unsafe { ffi::babl_format_get_bytes_per_pixel(self.0) }
    }

    #[doc(alias = "get_encoding")]
    #[doc(alias = "babl_format_get_encoding")]
    pub fn encoding(&self) -> String {
        unsafe {
            CStr::from_ptr(ffi::babl_format_get_encoding(self.0))
                .to_string_lossy()
                .into()
        }
    }

    #[doc(alias = "get_model")]
    #[doc(alias = "babl_format_get_model")]
    pub fn model(&self) -> Model {
        unsafe { Model::from_raw_full(ffi::babl_format_get_model(self.0)) }
    }

    #[doc(alias = "get_space")]
    #[doc(alias = "babl_format_get_space")]
    pub fn space(&self) -> Space {
        unsafe { Space::from_raw_full(ffi::babl_format_get_space(self.0)) }
    }

    #[doc(alias = "get_type")]
    #[doc(alias = "babl_format_get_type")]
    pub fn type_(&self, component_index: i32) -> Type {
        unsafe { Type::from_raw_full(ffi::babl_format_get_type(self.0, component_index)) }
    }

    #[doc(alias = "get_n_components")]
    #[doc(alias = "babl_format_get_n_components")]
    pub fn n_components(&self) -> i32 {
        unsafe { ffi::babl_format_get_n_components(self.0) }
    }

    #[doc(alias = "babl_format_has_alpha")]
    pub fn has_alpha(&self) -> bool {
        unsafe { ffi::babl_format_has_alpha(self.0) == 1 }
    }

    #[doc(alias = "babl_format_is_format_n")]
    pub fn is_format_n(&self) -> bool {
        unsafe { ffi::babl_format_is_format_n(self.0) == 1 }
    }

    #[doc(alias = "babl_format_is_palette")]
    pub fn is_palette(&self) -> bool {
        unsafe { ffi::babl_format_is_palette(self.0) == 1 }
    }

    #[doc(alias = "babl_format_n")]
    pub fn format_n(type_: &Type, components: i32) -> Self {
        unsafe { Self::from_raw_full(ffi::babl_format_n(type_.inner(), components)) }
    }
}

#[cfg(test)]
mod tests {
    use super::Format;
    use crate::init;
    use crate::ObjectType;

    #[test]
    fn basic_format() {
        init();

        let color_format = Format::from_encoding("CMYK float");

        assert_eq!(color_format.bytes_per_pixel(), 16);
        assert_eq!(color_format.n_components(), 4);
        assert_eq!(color_format.is_palette(), false);
        assert_eq!(color_format.is_format_n(), false);
        assert_eq!(color_format.has_alpha(), false);
        assert_eq!(color_format.type_(1).name(), "float");

        assert_eq!(color_format.name(), "CMYK float");
        assert_eq!(color_format.encoding(), "CMYK float");
    }

    #[test]
    fn format_that_requires_proper_null_termination() {
        init();

        let color_format = Format::from_encoding("cairo-ARGB32");

        assert_eq!(color_format.bytes_per_pixel(), 4);
        assert_eq!(color_format.n_components(), 4);
        assert_eq!(color_format.is_palette(), false);
        assert_eq!(color_format.is_format_n(), false);
        assert_eq!(color_format.has_alpha(), true);
        assert_eq!(color_format.type_(1).name(), "u8");

        assert_eq!(color_format.name(), "cairo-ARGB32");
        assert_eq!(color_format.encoding(), "cairo-ARGB32");
    }

    #[test]
    fn test_format_exists() {
        init();

        assert_eq!(Format::exists("CMYK float"), true);

        assert_eq!(Format::exists("CMYK invalid"), false);
    }
}
