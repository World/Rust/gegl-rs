pub use ffi;

#[doc(alias = "BABL_ALPHA_FLOOR")]
pub const ALPHA_FLOOR: f64 = ffi::BABL_ALPHA_FLOOR;
#[doc(alias = "BABL_ALPHA_FLOOR_F")]
pub const ALPHA_FLOOR_F: f64 = ffi::BABL_ALPHA_FLOOR_F;

#[macro_use]
mod object;

mod component;
mod enums;
mod format;
mod functions;
mod model;
mod sampling;
mod space;
mod trc;
mod types;

pub use component::Component;
pub use enums::{Error, IccIntent, ModelFlag, SpaceFlags};
pub use format::Format;
pub use functions::*;
pub use model::Model;
pub use object::ObjectType;
pub use sampling::Sampling;
pub use space::Space;
pub use trc::Trc;
pub use types::Type;
