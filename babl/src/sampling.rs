use crate::ObjectType;

define_object!(Sampling);

impl Sampling {
    #[doc(alias = "babl_sampling")]
    pub fn new(horizontal: i32, vertical: i32) -> Self {
        unsafe { Self::from_raw_full(ffi::babl_sampling(horizontal, vertical)) }
    }
}
