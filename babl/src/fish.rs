use crate::Object;

pub enum Performance {
    Exact,
    Precise,
    Fast,
    Glitch,
}

define_object!(Fish);

impl Fish {
    #[doc(alias = "babl_fish")]
    pub fn new(source: &Format, destination: &Format) -> Self {
        unsafe { Self::from_raw_full(ffi::babl_fish(source.inner(), destination.inner())) }
    }

    #[doc(alias = "babl_fast_fish")]
    pub fn with_performance(source: &Format, destination: &Format, performance: &str) -> Self {
        unsafe {
            Self::from_raw_full(ffi::babl_fast_fish(
                source.inner(),
                destination.inner(),
                performance,
            ))
        }
    }

    #[doc(alias = "babl_process")]
    pub fn process(&self, source: &Format, destination: &Format, n: u32) -> u32 {
        unsafe { ffi::babl_process(self.0, source.inner(), destination.inner(), n) }
    }

    #[doc(alias = "babl_process_rows")]
    pub fn process_rows(
        &self,
        source: &Format,
        source_stride: u32,
        dest: &Format,
        dest_stride: u32,
        n: u32,
        rows: u32,
    ) -> u32 {
        unsafe {
            ffi::babl_process_rows(
                self.0,
                source.inner(),
                source_stride,
                destination.inner(),
                dest_stride,
                n,
                rows,
            )
        }
    }
}
